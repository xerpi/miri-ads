#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <time.h>
#define EOS '\0'

#define WORD_SIZE 32
#define ASIZE 4 // Genetic alphabet

static inline void error(const char *s)
{
	printf("Error: %s\n", s);
	exit(-1);
}

static void normalize(const char *gentext, char *out, unsigned int size)
{
	for (unsigned int i = 0; i < size; i++) {
		char c = gentext[i];
		if (c == 'a' || c == 'A')
			out[i] = 0;
		else if (c == 'c' || c == 'C')
			out[i] = 1;
		else if (c == 'g' || c == 'G')
			out[i] = 2;
		else if (c == 't' || c == 'T')
			out[i] = 3;
	}
}

long readfile(char *filename, char *text[])
{
	FILE *fin;
	long len;
	int i = 0;
	char c;

	if (!(fin = fopen(filename, "r"))) {
		fprintf (stderr, "I can't open the file: %s\n", filename);
		exit(1);
	}
	// llegim el nombre de caracters que hi ha al fitxer
	fseek(fin, 0, SEEK_END);
	len = ftell(fin);
	fseek(fin, 0, SEEK_SET);
	// demanem memoria per la variable text
	*text = malloc(len + 1);
        // bucle per llegir el fitxer
	while ((c = (fgetc(fin))) != EOF) {
	  if ((c=='a')||(c=='c')||(c=='g')||(c=='t')||(c=='A')||(c=='C')||(c=='G')||(c=='T')){
	    (*text)[i] = c;
	    //printf("text[%d] = %c\n", i, (*text)[i]);
	    i++;
	  }
	}
	fclose(fin);
	return i;
}

uint64_t BNDM(const char *x, int m, const char *y, int n)
{
	unsigned int B[ASIZE];
	int i, j, last;
	unsigned int s, d;
	uint64_t nfound = 0;
	if (m > WORD_SIZE)
		error("BNDM");

	/* Pre processing */
	memset(B,0,ASIZE*sizeof(*B));
	s=1;
	for (i=m-1; i>=0; i--){
		B[(unsigned int)x[i]] |= s;
		s <<= 1;
	}

	/* Searching phase */
	j=0;
	while (j <= n-m){
		i=m-1; last=m;
		d = ~0U;
		while (i>=0 && d!=0) {
			d &= B[(unsigned int)y[j+i]];
			i--;
			if (d != 0) {
				if (i >= 0) {
					last = i+1;
				} else {
					//OUTPUT(j);
					nfound++;
				}
			}
			d <<= 1;
		 }
		 j += last;
	}

	return nfound;
}

int main(int argc, char *argv[])
{
        char pattern[256];
	char filename[256];
	char *text;
	int  n;
	clock_t initemps, end;

	sprintf(pattern,"%s",argv[2]);
	sprintf(filename,"%s",argv[1]);
	printf("pattern: %s \n",pattern);

	size_t pattern_len = strlen(pattern);

	char patter_norm[256];
	normalize(pattern, patter_norm, pattern_len);

	n = readfile(filename, &text);
	//printf("text: %s \n",text);
	//printf("n=%d\n",n);

	char *text_norm = malloc(n * sizeof(char));
	normalize(text, text_norm, n);

	printf("Searching...\n");
	initemps=clock();
	uint64_t found = BNDM(patter_norm,pattern_len,text_norm,n);
	end = clock();

	printf("Found %" PRIu64 "\n", found);
	printf("Seconds %f\n", (end-initemps)/(double)CLOCKS_PER_SEC);
	free(text);
	free(text_norm);

        return 0;
}
