#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <time.h>
#define ASIZE 256

void preBmBc(const char *x, int m, int bmBc[]) {
   int i;

   for (i = 0; i < ASIZE; ++i)
      bmBc[i] = m;
   for (i = 0; i < m - 1; ++i)
      bmBc[(unsigned int)x[i]] = m - i - 1;
}

long readfile(char *filename, char *text[])
{
	FILE *fin;
	long len;
	int i = 0;
	char c;

	if (!(fin = fopen(filename, "r"))) {
		fprintf (stderr, "I can't open the file: %s\n", filename);
		exit(1);
	}
	// llegim el nombre de caracters que hi ha al fitxer
	fseek(fin, 0, SEEK_END);
	len = ftell(fin);
	fseek(fin, 0, SEEK_SET);
	// demanem memoria per la variable text
	*text = malloc(len + 1);
        // bucle per llegir el fitxer
	while ((c = (fgetc(fin))) != EOF) {
	  if ((c=='a')||(c=='c')||(c=='g')||(c=='t')||(c=='A')||(c=='C')||(c=='G')||(c=='T')){
	    (*text)[i] = c;
	    //printf("text[%d] = %c\n", i, (*text)[i]);
	    i++;
	  }
	}
	fclose(fin);
	return i;
}

uint64_t HORSPOOL(const char *x, int m, const char *y, int n) {
   int j, bmBc[ASIZE];
   char c;
   uint64_t nfound = 0;

   /* Preprocessing */
    preBmBc(x, m, bmBc);

   /* Searching */
   j = 0;
   while (j <= n - m) {
      c = y[j + m - 1];
      if (x[m - 1] == c && memcmp(x, y + j, m - 1) == 0) {
	//printf("j=%d \n",j);
	nfound++;
      }
      j += bmBc[(unsigned int)c];
   }

   return nfound;
}

int main(int argc, char *argv[])
{
        char pattern[256];
	char filename[256];
	char *text;
	int  n;
	clock_t initemps, end;

	sprintf(pattern,"%s",argv[2]);
	sprintf(filename,"%s",argv[1]);
	printf("pattern: %s \n",pattern);

	size_t pattern_len = strlen(pattern);

	n = readfile(filename, &text);
	//printf("text: %s \n",text);
	//printf("n=%d\n",n);

	printf("Searching...\n");
	initemps = clock();
	uint64_t found = HORSPOOL(pattern,pattern_len,text,n);
	end = clock();

	printf("Found %" PRIu64 "\n", found);
	printf("Seconds %f\n", (end-initemps)/(double)CLOCKS_PER_SEC);

	free(text);
        return 0;
}
