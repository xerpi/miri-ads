#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <time.h>
#include <math.h>
#include "algorithms.c"

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(*x))

#define SEED		42
#define NUM_RUNS	5

static inline void random_gen(char *text, uint64_t size)
{
	static const char bases[] = {'A', 'G', 'T', 'C'};

	for (uint64_t i = 0; i < size; i++)
		text[i] = bases[rand() % ARRAY_SIZE(bases)];
}

int main(int argc, char *argv[])
{
	char filename[256];
	char *text;
	int  n;
	clock_t tinit, tend;
	uint64_t found;

	srand(time(NULL));

	sprintf(filename,"%s",argv[1]);

	n = readfile(filename, &text);
	//printf("text: %s \n",text);
	//printf("n=%d\n",n);

	char *text_norm = malloc(n * sizeof(char));
	normalize(text, text_norm, n);

	for (uint64_t i = 1; i <= 16; i++) {
		char pattern[256];
		random_gen(pattern, i);

		char patter_norm[256];
		normalize(pattern, patter_norm, i);

		printf("bruteforce\t%" PRIu64, i);
		for (uint64_t j = 0; j < NUM_RUNS; j++) {
			tinit = clock();
			found = BF(patter_norm, i, text_norm, n);
			tend = clock();
			if (j == 0)
				printf("\t%" PRIu64, found);
			printf("\t%f", (tend - tinit) / (double)CLOCKS_PER_SEC);
		}
		printf("\n");

		printf("horspool\t%" PRIu64, i);
		for (uint64_t j = 0; j < NUM_RUNS; j++) {
			tinit = clock();
			found = HORSPOOL(patter_norm, i, text_norm, n);
			tend = clock();
			if (j == 0)
				printf("\t%" PRIu64, found);
			printf("\t%f", (tend - tinit) / (double)CLOCKS_PER_SEC);
		}
		printf("\n");

		printf("BNDM\t%" PRIu64, i);
		for (uint64_t j = 0; j < NUM_RUNS; j++) {
			tinit = clock();
			found = BNDM(patter_norm, i, text_norm, n);
			tend = clock();
			if (j == 0)
				printf("\t%" PRIu64, found);
			printf("\t%f", (tend - tinit) / (double)CLOCKS_PER_SEC);
		}
		printf("\n");

		printf("BOM\t%" PRIu64, i);
		for (uint64_t j = 0; j < NUM_RUNS; j++) {
			tinit = clock();
			found = BOM(patter_norm, i, text_norm, n);
			tend = clock();
			if (j == 0)
				printf("\t%" PRIu64, found);
			printf("\t%f", (tend - tinit) / (double)CLOCKS_PER_SEC);
		}
		printf("\n");
	}

	free(text);
	free(text_norm);

        return 0;
}
