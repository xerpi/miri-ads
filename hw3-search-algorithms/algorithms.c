#define FALSE      0
#define TRUE       1

#define XSIZE     4096
#define UNDEFINED -1
#define WORD_SIZE 32
#define ASIZE     4 // Genetic alphabet

struct _cell {
    int element;
    struct _cell *next;
};

typedef struct _cell *List;

static inline void error(const char *s)
{
	printf("Error: %s\n", s);
	exit(-1);
}

static void normalize(const char *gentext, char *out, unsigned int size)
{
	for (unsigned int i = 0; i < size; i++) {
		char c = gentext[i];
		if (c == 'a' || c == 'A')
			out[i] = 0;
		else if (c == 'c' || c == 'C')
			out[i] = 1;
		else if (c == 'g' || c == 'G')
			out[i] = 2;
		else if (c == 't' || c == 'T')
			out[i] = 3;
	}
}

long readfile(char *filename, char *text[])
{
	FILE *fin;
	long len;
	size_t i, j = 0;
	char c;
	size_t numread;
	char buffer[4096];

	if (!(fin = fopen(filename, "r"))) {
		fprintf (stderr, "I can't open the file: %s\n", filename);
		exit(1);
	}
	// llegim el nombre de caracters que hi ha al fitxer
	fseek(fin, 0, SEEK_END);
	len = ftell(fin);
	fseek(fin, 0, SEEK_SET);
	// demanem memoria per la variable text
	*text = malloc(len + 1);

        // bucle per llegir el fitxer
	while ((numread = fread(buffer, 1, sizeof(buffer), fin)) > 0) {
		for (i = 0; i < numread; i++) {
			c = buffer[i];
			if ((c=='a')||(c=='c')||(c=='g')||(c=='t')||(c=='A')||(c=='C')||(c=='G')||(c=='T')) {
				(*text)[j] = c;
				//printf("text[%d] = %c\n", i, (*text)[i]);
				j++;
			}
		}
	}
	(*text)[j] = '\0';
	fclose(fin);
	return j;
}

/****************************** bruteforce ******************************/

uint64_t BF(const char *x, int m, const char *y, int n) {
	/* Searching */
	uint64_t nfound=0;
	for (uint64_t i = 0; i < n - m; i++, y++) {
		if (memcmp(x, y, m) == 0)
			nfound++;
	}
	return nfound;
}

/****************************** horspool ******************************/

void preBmBc(const char *x, int m, int bmBc[]) {
   int i;

   for (i = 0; i < ASIZE; ++i)
      bmBc[i] = m;
   for (i = 0; i < m - 1; ++i)
      bmBc[(unsigned int)x[i]] = m - i - 1;
}

uint64_t HORSPOOL(const char *x, int m, const char *y, int n) {
   int j, bmBc[ASIZE];
   char c;
   uint64_t nfound = 0;

   /* Preprocessing */
    preBmBc(x, m, bmBc);

   /* Searching */
   j = 0;
   while (j <= n - m) {
      c = y[j + m - 1];
      if (x[m - 1] == c && memcmp(x, y + j, m - 1) == 0) {
	//printf("j=%d \n",j);
	nfound++;
      }
      j += bmBc[(unsigned int)c];
   }

   return nfound;
}

/****************************** BOM ******************************/

int getTransition(const char *x, int p, List L[], char c) {
   List cell;

   if (p > 0 && x[p - 1] == c)
      return(p - 1);
   else {
      cell = L[p];
      while (cell != NULL)
         if (x[cell->element] == c)
            return(cell->element);
         else
            cell = cell->next;
      return(UNDEFINED);
   }
}


void setTransition(int p, int q, List L[]) {
   List cell;

   cell = (List)malloc(sizeof(struct _cell));
   if (cell == NULL)
      error("BOM/setTransition");
   cell->element = q;
   cell->next = L[p];
   L[p] = cell;
}


void oracle(const char *x, int m, char T[], List L[]) {
   int i, p, q = 0;
   int S[XSIZE + 1];
   char c;

   S[m] = m + 1;
   for (i = m; i > 0; --i) {
      c = x[i - 1];
      p = S[i];
      while (p <= m && (q = getTransition(x, p, L, c)) == UNDEFINED) {
         setTransition(p, i - 1, L);
         p = S[p];
      }
      S[i - 1] = (p == m + 1 ? m : q);
   }
   p = 0;
   while (p <= m) {
      T[p] = TRUE;
      p = S[p];
   }
}


uint64_t BOM(const char *x, int m, const char *y, int n) {
   char T[XSIZE + 1];
   List L[XSIZE + 1];
   int i, j, p, period = 0, q, shift;
   uint64_t nfound = 0;

   /* Preprocessing */
   memset(L, 0, (m + 1)*sizeof(List));
   memset(T, FALSE, (m + 1)*sizeof(char));
   oracle(x, m, T, L);

   /* Searching */
   j = 0;
   while (j <= n - m) {
      i = m - 1;
      p = m;
      shift = m;
      while (i + j >= 0 && (q = getTransition(x, p, L, y[i + j])) != UNDEFINED) {
         p = q;
         if (T[p] == TRUE) {
            period = shift;
            shift = i;
         }
         --i;
      }
      if (i < 0) {
         //OUTPUT(j);
	 nfound++;
         shift = period;
      }
      j += shift;
   }

   return nfound;
}

/****************************** BNDM ******************************/

uint64_t BNDM(const char *x, int m, const char *y, int n)
{
	unsigned int B[ASIZE];
	int i, j, last;
	unsigned int s, d;
	uint64_t nfound = 0;
	if (m > WORD_SIZE)
		error("BNDM");

	/* Pre processing */
	memset(B,0,ASIZE*sizeof(*B));
	s=1;
	for (i=m-1; i>=0; i--){
		B[(unsigned int)x[i]] |= s;
		s <<= 1;
	}

	/* Searching phase */
	j=0;
	while (j <= n-m){
		i=m-1; last=m;
		d = ~0U;
		while (i>=0 && d!=0) {
			d &= B[(unsigned int)y[j+i]];
			i--;
			if (d != 0) {
				if (i >= 0) {
					last = i+1;
				} else {
					//OUTPUT(j);
					nfound++;
				}
			}
			d <<= 1;
		 }
		 j += last;
	}

	return nfound;
}
