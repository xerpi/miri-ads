#ifndef PCSA_HPP
#define PCSA_HPP

#include <array>
#include <bitset>
#include <cmath>
#include "ICountDistinctAlgorithm.hpp"

namespace PCSA {

static constexpr std::size_t calc_L(std::size_t nmax, std::size_t m)
{
	return std::ceil(std::log2(nmax / m)) + 4;
}

static constexpr std::size_t calc_m(double error)
{
	return std::size_t(std::ceil(std::pow(0.78 / error, 2)));
}

template <typename T, std::size_t m, std::size_t L>
class PCSA : public ICountDistinctAlgorithm<T> {
public:
	PCSA() { }

	void add(const T& val)
	{
		uint32_t h = std::hash<T>{}(val);
		std::size_t alpha = h % m;
		int k = rho(h / m);
		bitmaps[alpha][k] = 1;
	}

	uint64_t count() const
	{
		uint64_t S = 0;
		for (auto &b: bitmaps)
			S += bitmap_trailing_ones(b);
		double A = S / double(m);
		return std::floor((m / PHI) * std::pow(2, A));
	}

	void merge(const ICountDistinctAlgorithm<T> &other)
	{
		for (std::size_t i = 0; i < m; i++)
			bitmaps[i] |= static_cast<PCSA<T, m, L> const&>(other).bitmaps[i];
	}

	void reset()
	{
		for (auto &b: bitmaps)
			b.reset();
	}

	std::string name()
	{
		return std::string("PCSA");
	}

private:
	static int rho(uint32_t n)
	{
		return trailing_zeroes(n);
	}

	static std::size_t bitmap_trailing_ones(const std::bitset<L> &bitmap)
	{
		std::size_t n = 0;
		while (bitmap[n] == 1 && n < L)
			n++;
		return n;
	}

	static constexpr double PHI = 0.77351;

	std::array<std::bitset<L>, m> bitmaps;
};

};

#endif
