#ifndef HYPERLOGLOG_HPP
#define HYPERLOGLOG_HPP

#include <cmath>
#include <array>
#include <algorithm>
#include "bitutils.hpp"
#include "ICountDistinctAlgorithm.hpp"

namespace HyperLogLog {

static constexpr std::size_t calc_m(double error)
{
	return std::size_t(std::ceil(std::pow(1.04 / error, 2)));
}

template <typename T, std::size_t m>
class HyperLogLog : public ICountDistinctAlgorithm<T> {
public:
	HyperLogLog()
	{
		alpha_m = calc_alpha_m();

		reset();
	}

	void add(const T& val)
	{
		uint32_t h = std::hash<T>{}(val);
		std::size_t j = h % m;
		int r = rho(h / m);
		if (r > M[j])
			M[j] = r;
	}

	uint64_t count() const
	{
		double Z = 0.0;
		for (const auto &bucket: M)
			Z += (1.0 / std::pow(2.0, bucket));

		double E = (alpha_m * m * m) / Z;
		double Es = E;
		if (E <= (5.0 * m / 2)) { // small range correction
			std::size_t V = 0;
			for (const auto &bucket: M) {
				if (bucket == 0)
					V++;
			}
			if (V != 0)
				Es = m * std::log(m / double(V));
			else
				Es = E;
		} else if (E <= (1 / 30.0)*std::pow(2.0, 32.0)) { // intermediate range—no correction
			Es = E;
		} else if (E > (1 / 30.0)*std::pow(2.0, 32.0)) { // large range correction
			Es = -std::pow(2.0, 32.0) * std::log(1.0 - E/std::pow(2.0, 32.0));
		}

		return Es;
	}

	void merge(const ICountDistinctAlgorithm<T> &other)
	{
		const HyperLogLog other_loglog = static_cast<HyperLogLog<T, m> const&>(other);
		for (std::size_t i = 0; i < m; i++) {
			if (other_loglog.M[i] > M[i])
				M[i] = other_loglog.M[i];
		}
	}

	void reset()
	{
		for (auto &bucket: M)
			bucket = 0;
	}

	std::string name()
	{
		return std::string("HyperLogLog");
	}

private:
	static int rho(uint32_t n)
	{
		int zeroes = trailing_zeroes(n);
		if (zeroes < 32)
			return zeroes + 1;
		else
			return zeroes;
	}

	static double calc_alpha_m(void)
	{
		if (m == 16)
			return 0.673;
		else if (m == 32)
			return 0.697;
		else if (m == 64)
			return 0.709;
		else
			return 0.7213 / (1 + 1.079/m);
	}

	double alpha_m;
	std::array<int, m> M;
};

};

#endif
