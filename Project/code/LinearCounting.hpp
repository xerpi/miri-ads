#ifndef LINEARCOUNTING_HPP
#define LINEARCOUNTING_HPP

#include <bitset>
#include <cmath>
#include "ICountDistinctAlgorithm.hpp"

template <typename T, std::size_t m>
class LinearCounting : public ICountDistinctAlgorithm<T> {
public:
	LinearCounting() { }

	void add(const T& val)
	{
		bitmap[std::hash<T>{}(val) % m] = 1;
	}

	uint64_t count() const
	{
		std::size_t U = m - bitmap.count();
		double V = U / double(m);
		double n = -double(m) * std::log(V);
		return std::round(n);
	}

	void merge(const ICountDistinctAlgorithm<T> &other)
	{
		bitmap |= static_cast<LinearCounting<T, m> const&>(other).bitmap;
	}

	void reset()
	{
		bitmap.reset();
	}

	std::string name()
	{
		return std::string("LinearCounting");
	}

private:
	std::bitset<m> bitmap;
};

#endif
