#ifndef BITUTILS_HPP
#define BITUTILS_HPP

#define trailing_zeroes(x) __builtin_ctz((x))
#define trailing_ones(x)   __builtin_ctz(~(x))

#endif
