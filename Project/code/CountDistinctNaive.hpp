#ifndef COUNTDISTINCTNAIVE_HPP
#define COUNTDISTINCTNAIVE_HPP

#include <unordered_set>
#include "ICountDistinctAlgorithm.hpp"

template <typename T>
class CountDistinctNaive : public ICountDistinctAlgorithm<T> {
public:
	CountDistinctNaive() : counter(0) { }

	void add(const T& val)
	{
		if (set.find(val) == set.end()) {
			set.insert(val);
			counter++;
		}
	}

	uint64_t count() const
	{
		return counter;
	}

	void merge(const ICountDistinctAlgorithm<T> &other)
	{
		for (const auto &e: static_cast<CountDistinctNaive<T> const&>(other).set)
			add(e);
	}

	void reset()
	{
		set.clear();
		counter = 0;
	}

	std::string name()
	{
		return std::string("Naive");
	}

private:
	std::unordered_set<T> set;
	uint64_t counter;
};

#endif
