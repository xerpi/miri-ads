#include <iostream>
#include <cinttypes>
#include <cstring>
#include <cstdio>
#include <fstream>
#include <chrono>
#include <random>
#include "CountDistinctNaive.hpp"
#include "LinearCounting.hpp"
#include "FlajoletMartin.hpp"
#include "PCSA.hpp"
#include "LogLog.hpp"
#include "SuperLogLog.hpp"
#include "HyperLogLog.hpp"

#define NMAX (uint64_t((1.5 * 10000000ULL)))

using namespace std;

template <typename T>
static std::pair<double, uint64_t> run_algorithm(ICountDistinctAlgorithm<T> &algo, const std::vector<T> &data)
{
	chrono::high_resolution_clock::time_point start_time;
	chrono::high_resolution_clock::time_point end_time;
	chrono::duration<double> duration;
	double elapsed_time;
	uint64_t cardinality;

	start_time = chrono::high_resolution_clock::now();
	for (const auto &x: data)
		algo.add(x);
	cardinality = algo.count();
	end_time = chrono::high_resolution_clock::now();

	algo.reset();

	duration = chrono::duration_cast<chrono::nanoseconds>(end_time - start_time);
	elapsed_time = duration.count();

	return std::pair<double, uint64_t>(elapsed_time, cardinality);
}

template <typename T>
static int run_rand_experiment_inner(const std::vector<ICountDistinctAlgorithm<T> *> &algos)
{
	/* Random generators */
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<T> dis;

	/* Vectors for the algorithm results */
	std::vector<std::pair<double, uint64_t>> results[algos.size()];

	/* Increasing size with random data */
	constexpr uint64_t RAND_INITIAL_SIZE = 100000;
	constexpr uint64_t RAND_MAX_SIZE     = 10000000ULL;
	constexpr uint64_t RAND_STEP         = 100000;

	/* Vector for the random data */
	std::vector<T> data;

	uint64_t rand_cur_size = RAND_INITIAL_SIZE;
	while (rand_cur_size <= RAND_MAX_SIZE) {
		//printf("Size: %" PRIu64 "\n", rand_cur_size);

		data.resize(rand_cur_size);

		/* Initialize the vector with random data */
		for (std::size_t i = 0; i < data.size(); i++)
			data[i] = dis(gen);

		/* Run the algorithms */
		for (uint64_t i = 0; i < algos.size(); i++)
			results[i].push_back(run_algorithm(*algos[i], data));

		rand_cur_size += RAND_STEP;
	}

	printf("Size");
	for (uint64_t i = RAND_INITIAL_SIZE; i <= RAND_MAX_SIZE; i += RAND_STEP)
		printf("\t%" PRIu64, i);
	printf("\n");
	for (uint64_t i = 0; i < algos.size(); i++) {
		printf("%-8s", algos[i]->name().c_str());
		for (std::size_t j = 0; j < results[i].size(); j++) {
			printf("\t%.08f", results[i][j].first);
		}
		printf("\n");
	}

	printf("\n");

	printf("Size");
	for (uint64_t i = RAND_INITIAL_SIZE; i <= RAND_MAX_SIZE; i += RAND_STEP)
		printf("\t%" PRIu64, i);
	printf("\n");
	for (uint64_t i = 0; i < algos.size(); i++) {
		printf("%-8s", algos[i]->name().c_str());
		for (std::size_t j = 0; j < results[i].size(); j++) {
			printf("\t%" PRIu64, results[i][j].second);
		}
		printf("\n");
	}

	return 0;
}

template <typename T>
static int run_text_experiment_inner(const std::vector<ICountDistinctAlgorithm<T> *> &algos,
                                     const std::string &dir,
                                     const std::vector<std::string> &filenames)
{
	/* Vectors for the algorithm results */
	std::vector<std::pair<double, uint64_t>> results[algos.size()];

	for (const auto &filename: filenames) {
		/* Open the file */
		std::string fullname = dir + "/" + filename;
		std::ifstream file(fullname);
		if (!file.is_open()) {
			printf("Error opening \"%s\".\n", fullname.c_str());
			return EXIT_FAILURE;
		}

		/* Read all the words of the file and put them in a vector */
		std::vector<std::string> data;
		std::string word;
		while (file >> word)
			data.push_back(word);

		/* Run the algorithms */
		for (uint64_t i = 0; i < algos.size(); i++)
			results[i].push_back(run_algorithm(*algos[i], data));
	}

	printf("File");
	for (const auto &filename: filenames)
		printf("\t%s", filename.c_str());

	printf("\n");
	for (uint64_t i = 0; i < algos.size(); i++) {
		printf("%-8s", algos[i]->name().c_str());
		for (std::size_t j = 0; j < results[i].size(); j++) {
			printf("\t%.08f", results[i][j].first);
		}
		printf("\n");
	}

	printf("\n");

	printf("File");
	for (const auto &filename: filenames)
		printf("\t%s", filename.c_str());
	printf("\n");
	for (uint64_t i = 0; i < algos.size(); i++) {
		printf("%-8s", algos[i]->name().c_str());
		for (std::size_t j = 0; j < results[i].size(); j++) {
			printf("\t%" PRIu64, results[i][j].second);
		}
		printf("\n");
	}

	return 0;
}

template <typename T>
static int run_rand_experiment(void)
{
	CountDistinctNaive<T> naive;
	LinearCounting<T, NMAX> linearCounting;
	FlajoletMartin<T, 32> flajoletMartin;

	constexpr std::size_t pcsa_m = PCSA::calc_m(0.05);
	constexpr std::size_t pcsa_L = PCSA::calc_L(NMAX, pcsa_m);
	PCSA::PCSA<T, pcsa_m, pcsa_L> pcsa;

	constexpr std::size_t loglog_m = LogLog::calc_m(0.05);
	LogLog::LogLog<T, loglog_m> loglog;

	constexpr std::size_t superloglog_m = SuperLogLog::calc_m(0.05);
	SuperLogLog::SuperLogLog<T, superloglog_m> superLoglog(0.7, NMAX);

	constexpr std::size_t hyperloglog_m = HyperLogLog::calc_m(0.05);
	HyperLogLog::HyperLogLog<T, hyperloglog_m> hyperLoglog;

	static const std::vector<ICountDistinctAlgorithm<T> *> algos = {
		&naive,
		&linearCounting,
		&flajoletMartin,
		&pcsa,
		&loglog,
		&superLoglog,
		&hyperLoglog
	};

	return run_rand_experiment_inner<uint32_t>(algos);
}

template <typename T>
static int run_text_experiment(void)
{
	CountDistinctNaive<T> naive;
	LinearCounting<T, NMAX> linearCounting;
	FlajoletMartin<T, 32> flajoletMartin;

	constexpr std::size_t pcsa_m = PCSA::calc_m(0.05);
	constexpr std::size_t pcsa_L = PCSA::calc_L(NMAX, pcsa_m);
	PCSA::PCSA<T, pcsa_m, pcsa_L> pcsa;

	constexpr std::size_t loglog_m = LogLog::calc_m(0.05);
	LogLog::LogLog<T, loglog_m> loglog;

	constexpr std::size_t superloglog_m = SuperLogLog::calc_m(0.05);
	SuperLogLog::SuperLogLog<T, superloglog_m> superLoglog(0.7, NMAX);

	constexpr std::size_t hyperloglog_m = HyperLogLog::calc_m(0.05);
	HyperLogLog::HyperLogLog<T, hyperloglog_m> hyperLoglog;

	static const std::vector<ICountDistinctAlgorithm<T> *> algos = {
		&naive,
		&linearCounting,
		&flajoletMartin,
		&pcsa,
		&loglog,
		&superLoglog,
		&hyperLoglog
	};

	static const std::vector<std::string> filenames = {
		"dickens.txt",
		"leipzig1m.txt",
		"mobydick.txt",
		"t8.shakespeare.txt",
		"upc-items.csv"
	};

	return run_text_experiment_inner<std::string>(algos, "../data", filenames);
}

int main(int argc, char *argv[])
{
	int ret;

	ret = run_rand_experiment<uint32_t>();
	if (ret < 0)
		return ret;
	ret = run_text_experiment<std::string>();
	return ret;
}
