#ifndef SUPERLOGLOG_HPP
#define SUPERLOGLOG_HPP

#include <cmath>
#include <array>
#include <algorithm>
#include "bitutils.hpp"
#include "ICountDistinctAlgorithm.hpp"

namespace SuperLogLog {

static constexpr std::size_t calc_m(double error)
{
	return std::size_t(std::ceil(std::pow(1.05 / error, 2)));
}

template <typename T, std::size_t m>
class SuperLogLog : public ICountDistinctAlgorithm<T> {
public:
	SuperLogLog(double theta = 0.7, uint64_t nmax = 1000000000ULL)
	{
		m0 = std::floor(theta * m);
		B = std::ceil(std::log2(nmax / double(m)) + 3.0);

		double inv_m = -1.0 / m;
		double num = std::pow(2.0, inv_m) - 1.0;
		double den = std::log(2.0);
		alpha_m = std::pow(std::tgamma(inv_m) * (num / den), -double(m));

		reset();
	}

	void add(const T& val)
	{
		uint32_t h = std::hash<T>{}(val);
		std::size_t j = h % m;
		int r = rho(h / m);
		if (r > M[j])
			M[j] = r;
	}

	uint64_t count() const
	{
		std::array<int, m> M_sorted = M;
		std::sort(M_sorted.begin(), M_sorted.end());

		uint64_t s = 0;
		uint64_t num = 0;
		for (std::size_t i = 0; i < m0; i++) {
			if (M_sorted[i] <= B)
				s += M_sorted[i];
			else
				break;
			num++;
		}
		double average = s / double(num);
		return alpha_m * num * std::pow(2.0, average);
	}

	void merge(const ICountDistinctAlgorithm<T> &other)
	{
		const SuperLogLog other_loglog = static_cast<SuperLogLog<T, m> const&>(other);
		for (std::size_t i = 0; i < m; i++) {
			if (other_loglog.M[i] > M[i])
				M[i] = other_loglog.M[i];
		}
	}

	void reset()
	{
		for (auto &bucket: M)
			bucket = 0;
	}

	std::string name()
	{
		return std::string("SuperLogLog");
	}

private:
	static int rho(uint32_t n)
	{
		return trailing_zeroes(n);
	}

	double alpha_m;
	std::size_t m0;
	int B;
	std::array<int, m> M;
};

};

#endif
