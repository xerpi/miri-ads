#ifndef FLAJOLETMARTIN_HPP
#define FLAJOLETMARTIN_HPP

#include <bitset>
#include <cmath>
#include "bitutils.hpp"
#include "ICountDistinctAlgorithm.hpp"

template <typename T, std::size_t L>
class FlajoletMartin : public ICountDistinctAlgorithm<T> {
public:
	FlajoletMartin() { }

	void add(const T& val)
	{
		uint32_t h = std::hash<T>{}(val);
		int i = rho(h);
		bitmap[i] = 1;
	}

	uint64_t count() const
	{
		std::size_t R = bitmap_trailing_ones(bitmap);
		double M = std::pow(2.0, R) / PHI;
		return std::round(M);
	}

	void merge(const ICountDistinctAlgorithm<T> &other)
	{
		bitmap |= static_cast<FlajoletMartin<T, L> const&>(other).bitmap;
	}

	void reset()
	{
		bitmap.reset();
	}

	std::string name()
	{
		return std::string("FlajoletMartin");
	}

private:
	static int rho(uint32_t n)
	{
		return trailing_zeroes(n);
	}

	static std::size_t bitmap_trailing_ones(const std::bitset<L> &bitmap)
	{
		std::size_t n = 0;
		while (bitmap[n] == 1 && n < L)
			n++;
		return n;
	}

	static constexpr double PHI = 0.77351;

	std::bitset<L> bitmap;
};

#endif
