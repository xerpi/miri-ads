#ifndef LOGLOG_HPP
#define LOGLOG_HPP

#include <cmath>
#include <array>
#include "bitutils.hpp"
#include "ICountDistinctAlgorithm.hpp"

namespace LogLog {

static constexpr std::size_t calc_m(double error)
{
	return std::size_t(std::ceil(std::pow(1.30 / error, 2)));
}

template <typename T, std::size_t m>
class LogLog : public ICountDistinctAlgorithm<T> {
public:
	LogLog()
	{
		double inv_m = -1.0 / m;
		double num = std::pow(2.0, inv_m) - 1.0;
		double den = std::log(2.0);
		alpha_m = std::pow(std::tgamma(inv_m) * (num / den), -double(m));

		reset();
	}

	void add(const T& val)
	{
		uint32_t h = std::hash<T>{}(val);
		std::size_t j = h % m;
		int r = rho(h / m);
		if (r > M[j])
			M[j] = r;
	}

	uint64_t count() const
	{
		uint64_t s = 0;
		for (const auto &bucket: M)
			s += bucket;
		double average = s / double(m);
		return alpha_m * m * std::pow(2.0, average);
	}

	void merge(const ICountDistinctAlgorithm<T> &other)
	{
		const LogLog other_loglog = static_cast<LogLog<T, m> const&>(other);
		for (std::size_t i = 0; i < m; i++) {
			if (other_loglog.M[i] > M[i])
				M[i] = other_loglog.M[i];
		}
	}

	void reset()
	{
		for (auto &bucket: M)
			bucket = 0;
	}

	std::string name()
	{
		return std::string("LogLog");
	}

private:
	static int rho(uint32_t n)
	{
		return trailing_zeroes(n);
	}

	double alpha_m;
	std::array<int, m> M;
};

};

#endif
