#include <iostream>
#include <cinttypes>
#include <cstring>
#include <cstdio>
#include <fstream>
#include <chrono>
#include <random>
#include "CountDistinctNaive.hpp"
#include "LinearCounting.hpp"
#include "FlajoletMartin.hpp"
#include "PCSA.hpp"
#include "LogLog.hpp"
#include "SuperLogLog.hpp"
#include "HyperLogLog.hpp"

#define DEFAULT_RAND_NUM_ELEMENTS (uint64_t(1000000ULL))
#define NMAX                      (uint64_t((1.5 * DEFAULT_RAND_NUM_ELEMENTS)))

using namespace std;

static void usage(char *argv[]);

template <typename T>
static void run_algorithm(ICountDistinctAlgorithm<T> &algo, const std::vector<T> &data, uint64_t num_runs)
{
	chrono::high_resolution_clock::time_point start_time;
	chrono::high_resolution_clock::time_point end_time;
	chrono::duration<double> duration;
	uint64_t cardinality = 0;
	double elapsed_time = 0.0;

	for (uint64_t i = 0; i < num_runs; i++) {
		start_time = chrono::high_resolution_clock::now();
		for (const auto &x: data)
			algo.add(x);
		cardinality = algo.count();
		end_time = chrono::high_resolution_clock::now();

		algo.reset();

		duration = chrono::duration_cast<chrono::nanoseconds>(end_time - start_time);
		elapsed_time += duration.count();
	}

	printf("%-8s\t%10" PRIu64 "\t%.8f\n", algo.name().c_str(), cardinality, (elapsed_time / double(num_runs)));
}

template <typename T>
static void run_experiment(const std::vector<T> &data, uint64_t num_runs)
{
	CountDistinctNaive<T> naive;
	LinearCounting<T, NMAX> linearCounting;
	FlajoletMartin<T, 32> flajoletMartin;

	constexpr std::size_t pcsa_m = PCSA::calc_m(0.05);
	constexpr std::size_t pcsa_L = PCSA::calc_L(NMAX, pcsa_m);
	PCSA::PCSA<T, pcsa_m, pcsa_L> pcsa;

	constexpr std::size_t loglog_m = LogLog::calc_m(0.05);
	LogLog::LogLog<T, loglog_m> loglog;

	constexpr std::size_t superloglog_m = SuperLogLog::calc_m(0.05);
	SuperLogLog::SuperLogLog<T, superloglog_m> superLoglog(0.7, NMAX);

	constexpr std::size_t hyperloglog_m = HyperLogLog::calc_m(0.05);
	HyperLogLog::HyperLogLog<T, hyperloglog_m> hyperLoglog;

	printf("Input data size: %" PRIu64 "\n", data.size());
	printf("Number of runs: %" PRIu64 "\n", num_runs);

	run_algorithm(naive, data, num_runs);
	run_algorithm(linearCounting, data, num_runs);
	run_algorithm(flajoletMartin, data, num_runs);
	run_algorithm(pcsa, data, num_runs);
	run_algorithm(loglog, data, num_runs);
	run_algorithm(superLoglog, data, num_runs);
	run_algorithm(hyperLoglog, data, num_runs);
}

int main(int argc, char *argv[])
{
	bool rand_mode = true;
	const char *filename = nullptr;
	uint64_t rand_mode_num_elements = DEFAULT_RAND_NUM_ELEMENTS;
	uint64_t num_runs = 1;

	if (argc > 1) {
		if (argc <= 2) {
			usage(argv);
			return EXIT_FAILURE;
		} else if (strcmp("-r", argv[1]) == 0) {
			rand_mode = true;
		} else if (strcmp("-f", argv[1]) == 0) {
			rand_mode = false;
		} else {
			usage(argv);
			return EXIT_FAILURE;
		}
	}

	if (argc > 2) {
		if (rand_mode) {
			rand_mode_num_elements = std::stoull(argv[2]);
		} else {
			filename = argv[2];
		}
	}

	if (argc > 3)
		num_runs = std::stoull(argv[3]);

	if (rand_mode) {
		printf("Random mode with %" PRIu64 " elements.\n",
		       rand_mode_num_elements);

		/* Vector for the random data */
		std::vector<uint32_t> elements;
		elements.resize(rand_mode_num_elements);

		/* Initialize the vector with random data */
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<uint32_t> dis;
		for (std::size_t i = 0; i < elements.size(); i++)
			elements[i] = dis(gen);

		run_experiment(elements, num_runs);

	} else {
		printf("File mode with file \"%s\".\n", filename);

		std::ifstream file(filename);
		if (!file.is_open()) {
			printf("Error opening \"%s\".\n", filename);
			return EXIT_FAILURE;
		}

		/* Read all the words of the file and put them in a vector */
		std::vector<std::string> elements;
		std::string word;
		while (file >> word)
			elements.push_back(word);

		run_experiment(elements, num_runs);
	}

	return EXIT_SUCCESS;
}

static void usage(char *argv[])
{
	printf("count-distinct by Sergi Granell (sergi.granell@est.fib.upc.edu)\n");
	printf("Usage:\n");
	printf("\t%s <type> <arg> [num_runs]\n", argv[0]);
	printf("Where <type> can be:\n");
	printf("\t-r for random.      <arg> is the number of elements.\n");
	printf("\t-f for a text file. <arg> is the filename.\n");
	printf("\t-h to show this message and exit.\n");
	printf("And <num_runs> is the number of runs (arithmetic mean).\n");
	printf("The default is:\n");
	printf("\t%s -r %" PRIu64 " 1\n", argv[0], DEFAULT_RAND_NUM_ELEMENTS);
}
