#ifndef ICOUNTDISTINCTALGORITHM_HPP
#define ICOUNTDISTINCTALGORITHM_HPP

#include <cinttypes>
#include <string>

template <typename T>
class ICountDistinctAlgorithm {
public:
	virtual ~ICountDistinctAlgorithm() = default;
	virtual void add(const T& val) = 0;
	virtual uint64_t count() const = 0;
	virtual void merge(const ICountDistinctAlgorithm<T> &other) = 0;
	virtual void reset() = 0;
	virtual std::string name() = 0;
};

#endif
