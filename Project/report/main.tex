\documentclass[12pt,a4paper,oneside]{article}
\usepackage[utf8]{inputenc}
\usepackage[hidelinks]{hyperref}
\usepackage[toc,page]{appendix}
\usepackage[nottoc]{tocbibind}
\usepackage{parskip}
\usepackage{float}
\usepackage{url}
\usepackage{natbib}
\usepackage{graphicx}
\usepackage{titlesec}
\usepackage{minted}
\usepackage{csquotes}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage[linesnumbered,ruled,vlined]{algorithm2e}
\usepackage{listings}
\usepackage[space]{grffile}

\bibliographystyle{plainnat}

\SetKwInput{KwInput}{Input}
\SetKwInput{KwOutput}{Output}
\SetKw{Continue}{continue}
\SetKw{Return}{return}
\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}

\begin{document}

\pagenumbering{gobble}

\begin{titlepage}
	\centering
	\includegraphics[width=0.60\textwidth]{img/upc-positiu-p3005.png}
	\par
	\large Universitat Politècnica de Catalunya - BarcelonaTech
	\par
	\large Facultat d'Informàtica de Barcelona
	\par
	\vspace{1cm}
	\large Master in Innovation and Research in Informatics
	\par
	\large High Performance Computing
	\par
	\large 2018 Q2
	\par
	\vspace{0.75cm}
	\Large Advanced Data Structures
	\par
	\vspace{0.5cm}
	\LARGE \textbf{\textit{Count-distinct algorithms}}
	\par
	\vspace{2cm}
	\large \textit{Author:}
	\par
	\large Sergi Granell Escalfet
	\par
	\small sergi.granell@est.fib.upc.edu
	\par
	\vspace{1cm}
	\par
	\vfill
	\large \today \par
\end{titlepage}

\newpage
\tableofcontents
\newpage

\pagenumbering{arabic}

\section{Introduction}

In this project I will explain some of the algorithms (most of them probabilistic) that I have found in the literature regarding the count-distinct problem, or computing the cardinality of multisets.

At the end, I will also compare these algorithms by using my own implementations and explain the results and conclusions I reach.

\section{Count-distinct problem and its applications}

\subsection{The problem}

The count-distinct is a well known problem in computer science, the objective of which is to find the number of distinct elements in a data set or data stream, where some repeated elements might exist (also known as a multiset).

Determining the number of distinct elements in a data set/stream can be easily computed using space linear in the cardinality, for in many applications, this cost is impractical and requires too much memory. Therefore, an idea is to relax the constraint of computing the \textit{exact} cardinality, and instead use \textit{probabilistic} algorithms that estimate such cardinality approximately.

\subsection{Applications}

This problem arises in many areas of data-mining, database query optimization, web-page queries and visitor counts by their IP address, the analysis of traffic in routers, etc. In such contexts, data may be either too large to fit at once in memory or even too massive to be stored, being a huge continuous flow of data packets, therefore probabilistic but approximate algorithms, within a controllable error margin, are a need in those contexts.

\section{Algorithms for the count-distinct problem}

\subsection{Naive approach}

One of the first approaches one might think of is to calculate the \textit{exact number of distinct elements in a multiset} by keeping a set of all the previously seen elements, then for each $m \in M$, check if $m$ exists in this set and in case it does not, increase a a counter. After all the elements in $M$ have been handled, this counter will contain the number of distinct elements in $M$.

\begin{algorithm}[H] \label{algo:naive}
\SetAlgoLined
\DontPrintSemicolon
\KwInput{Stream of elements $M$}
\KwOutput{Number of different elements in $M$}
c $\leftarrow$ 0\;
S $\leftarrow \emptyset$\;
\For {each $m \in M$} {
    \If {$m \notin S$} {
        c $\leftarrow$ c + 1\;
        $S \leftarrow S \cup m$\;
    }
}
\Return c\;
\caption{Naive algorithm}
\end{algorithm}

Although this algorithm is very easy to implement, it requires keeping a dictionary data structure, such as search tree or a hash table, in which insertion and membership checking can be performed quickly.
Not only that, but this algorithm also has a high memory impact, specially when the number of distinct elements approaches the number of elements in $M$, since at this point we will have a full copy of $M$ plus the dictionary's data structure book-keeping metadata.

It is easy to see that the cost of using this algorithm is $m \cdot cost(dict\_search) + n \cdot cost(dict\_insert)$, where $n$ is the cardinality. On the other side, getting the (exact) cardinality has a constant cost (reading the counter).

\subsection{Linear counting} \label{subsection:linearcounting}

In the paper published by \citet{WhangBitmap}, they propose a technique, called linear counting, which is based on hashing and computes a \textit{non-exact estimation} of the distinct number of elements. The name \enquote{linear counting} comes from the fact that its memory requirement is almost linear in $n$ in order to obtain good estimation.

Other than being space efficient, a good feature of linear counting is that it allows to specify the desired level of accuracy, being useful when you need to control the error while keeping it space efficient.

The algorithm works in two-steps: 1) initialize a bitmap of zeros, and for each input data apply a hash function that maps it into a bit in the bitmap and sets it to 1, and 2) count the number of empty bits ($U_n$) in the bitmap and use it as input to an estimator for $n$: $\hat n$.

\citet{WhangBitmap} make the analysis of this method by recasting it into the \textit{urn problem} (explored in \cite{Zanden1986ESTIMATINGBA}), from which they realize the estimator:
$$\hat n = -m \cdot ln(V_n)$$

Where $m$ is the size of the bitmap and $V_n = U_n/m$ is the ratio of empty bits in the bitmap.

The following snippet shows the pseudocode of this algorithm:

\begin{algorithm}[H] \label{algo:linearcounting}
\SetAlgoLined
\DontPrintSemicolon
\KwInput{Stream of elements $Q$}
\KwInput{Size of the bitmap $m$}
\KwOutput{Estimator, $\hat n$, of the number of different elements in $Q$}
bitmap $\leftarrow$ $\{0\}^m$\;
\For {each $q \in Q$} {
    bitmap[hash($q$)] $\leftarrow$ 1\;
}
$U_n$ $\leftarrow$ number of \enquote{0}s in bitmap\;
$V_n $ $\leftarrow$ $U_n/m$\;
$\hat n \leftarrow -m \cdot ln(V_n)$\;
\Return $\hat n$\;
\caption{Linear counting}
\end{algorithm}

It is important to note that the size of the bitmap can be much smaller than the expected cardinality (then there will be collisions), depending on how much error we can tolerate.

The authors relate the bitmap size, with two constraints, the load factor ($t = n/m$) and the standard error of $U_n$ so that one can manipulate the parameters to influence its performance:
\begin{enumerate}
    \item Limit the standard error, defined as the standard deviation of $\hat n / n$, to $\epsilon$, given the load factor $t$:
        \begin{align*}
            m > \frac{e^t - t - 1}{(\epsilon t)^2}
        \end{align*}
    \item \label{linearcnt:c2} Make the probability of the bitmap becoming full negligible:
        \begin{align*}
            m > \beta(e^t - t - 1), \\
            \beta = max(a^2, 1/(\epsilon t)^2)
        \end{align*}
        Where $a$ is the least number of standard deviations that $E[U_n]$ has to be away from zero: $E(U_n) - a \cdot StdDev(U_n) > 0$. \\
        Since $U_n$ converges to a Poisson distribution, the probability of fill-up ($U_n = 0$) is:
            $$Pr(U_n = 0) = e^{- \lambda}$$
        Then we know that $E(U_n) > a \cdot StdDev(U_n)$, and since $E(U_n) = \lambda$ and $StdDev(U_n) = \sqrt{\lambda}$, we have $\sqrt{\lambda} > a$ which leads to:
            $$Pr(U_n = 0) < e^{-a^2}$$
\end{enumerate}

For the case of $a = \sqrt{5}$:
$$Pr(U_n = 0) < e^{-5} 	\approx 0.007 (0.7\%)$$

An interesting thing is that, as derived, this number is independent of $n$ or $m$.

Since the first equations shown in constraint \ref{linearcnt:c2} cannot be solved for $m$ analytically, the authors provide a table of precomputed solutions in Table II of their paper. This table has to be searched for the number of elements in $Q$ as $n$, since $n$ is not known before the measurement is completed. 

\if 0
To calculate a proper $m$, we should take into account the bias of the ratio $\hat n / n$, and its standard error:
\begin{enumerate}
    \item The bias is defined as the expected relative error of the estimator:
        $$Bias\left( \frac{\hat n}{n} \right) = E \left( \frac{\hat n}{n} \right) - 1 = \frac{e^t - t - 1}{2n}$$
    \item Limit the standard error, defined as the standard deviation of $\hat n / n$, to $\epsilon$, given the load factor $t = n/m$):
        $$m > \frac{e^t - t - 1}{(\epsilon t)^2}$$
    The load factor must be properly selected to achieve a specific bias. In the paper they show a graph with such relationship.
\end{enumerate}
\fi

Another feature of this algorithm is that load factors much higher than one (e.g., $12$) can be used while achieving good accuracy (e.g., $1$\%).

One of the downsides of this algorithm is that it might not be useful in streaming applications, where $Q$ is not known in advance or the number of elements cannot be counted until the end, since that would require extra storage. 

\subsection{Flajolet--Martin algorithm} \label{subsection:fm}

Another approach, also probabilistic and based on hashing, is the first algorithm \citep{FmAlgorithm} propose in their paper. An important feature is that it allows to count the number of unique objects in a stream or a set/database in one single pass. If the stream contains $n$ elements with $m$ of them unique, this algorithm runs in $O(n)$ time and only needs $O(log(m))$ memory.

Due to its probabilistic nature, the output is an approximation for the number of unique objects, along with a standard deviation $\sigma$, which could be used to determine bounds on the approximation with a desired maximum error $\epsilon$.

They key principle behind Flajolet--Martin algorithm's is to use hashes of elements as indicators for the number of distinct values in that collection (bit-pattern observables).

The basic observation is that if the values of $hash(x)$ are uniformly distributed, the pattern
$0^k1\cdots$ appears with probability $2^{-k-1}$:
\begin{align*}
&P(.....1)\ \ \, = 2^{-1} \\
&P(....10)\ \  = 2^{-2} \\
&P(...100)\ \, = 2^{-3} \\
& \qquad \qquad \vdots \\
&P(..10^{k-1}) = 2^{-k}
\end{align*}

The function $\rho(s)$ is introduced, for $s \in \{0, 1\}^L$, is the position of the least significant (rightmost) 1-bit, which is the number of trailing zeroes (e.g. $\rho(101000) = 3$), with $\rho(0) = L$.

The algorithm also uses a bitmap of length $L$, which is initialized to $0s$ at the beginning. Then it works as follows: for each input $x \in M$, the hash is applied and the number of trailing zeros is determined: $k = \rho(hash(x))$. If the number of trailing zeros is $k$, then we set the $k$-th bit in the bitmap to $1$.

At the end, let $R$ be the index of the first 0 in the bitmap; they propose to use that value as an indicator of $log_2n$. Since hashes values are uniformly distributed, the expected value of $R$ is close to:
$$E(R) \approx log_2(\phi n), \quad \phi = 0.77351\cdots$$

Therefore, by using the \enquote{correction factor} $\phi$ (derived in the paper by using asymptotic study), we can estimate the number of unique words $E^*$ as:
$$E^* = 2^R/\phi$$

\begin{algorithm}[H] \label{algo:fm}
\SetAlgoLined
\DontPrintSemicolon
\KwInput{Stream of elements $Q$}
\KwInput{Length of the bitmap $L$ (usually 64 is enough)}
\KwOutput{Estimator, $E^*$, of the number of different elements in $Q$}
bitmap $\leftarrow$ $\{0\}^L$\;
\For {each $q \in Q$} {
    $k$ $\leftarrow$ $\rho(hash(q))$\;
    bitmap[$k$] $\leftarrow 1$\;
}
$R$ $\leftarrow$ Smallest index $i$ such that bitmap[$i$]$=0$\;
$E^* \leftarrow 2^R/\phi$\;
\Return $E^*$\;
\caption{Flajolet--Martin algorithm}
\end{algorithm}

In the paper, Flajolet and Martin also show that the standard deviation of $R$ is close to:
$$\sigma(R) \approx 1.12$$
Which means that the estimate $E^*$ will typically be one binary order of magnitude off the exact result, and therefore, making clear that this algorithm needs an improvement, such as Probabilistic Counting with Stochastic Averaging, explained in the next section.

\subsection{Probabilistic Counting with Stochastic Averaging} \label{subsection:pcsa}

The dispersion of results of 1 binary order of magnitude of the plain Flajolet--Martin algorithm (subsection \ref{subsection:fm}) might be too high for many applications, therefore the same authors also propose an improvement over that algorithm in the \citep{FmAlgorithm} paper, which they call Probabilistic Counting with Stochastic Averaging (PCSA).

To remedy this, the authors propose to use a set $H$ of $m$ hashing functions, being $m$ a design parameter, and then computing $m$ different bitmaps.
Then we can obtain $m$ estimates $R^{<1>}, R^{<2>}, ..., R^{<m>}$, and take their average:
$$A = \frac{R^{<1>} + R^{<2>} + \cdots + R^{<m>}}{m}$$

Now if our input data has $n$ distinct elements, the random variable $A$ has the following expectation and standard deviation ($\sigma_\infty =  1.12127...$ is derived in the paper):
$$E(A) \approx log_2(\phi n), \quad \sigma(A) \approx \sigma_\infty / \sqrt{m}$$

Even though taking direct averaging as explained provides a good estimate (e.g. expected relative error of about 10\% if $m = 64$), it has the problem that it requires the calculation of many hashing functions, therefore the cost per element scanned gets multiplied by a factor of $m$.

To remedy this, the authors propose to take the \textit{stochastic average} instead: use a single hash function in order to distribute each record $x$ into one of $m$ lots, depending on the output bits: $\alpha = hash(x) \mod m$. The rest of the bits ($hash(x) \ div\  m \equiv \floor{hash(x) / m}$) can then be used to update the $\alpha$-th bitmap after applying $\rho$.

\begin{algorithm}[H] \label{algo:pcsa}
\SetAlgoLined
\DontPrintSemicolon
\KwInput{Stream of elements $Q$}
\KwInput{Number of the bitmaps $m$}
\KwInput{Length of the bitmaps $L$}
\KwOutput{Estimator, $E^*$, of the number of different elements in $Q$}
bitmap[i] $\leftarrow$ $\{0\}^L,\ 0 \leq i < m$\;
\For {each $q \in Q$} {
    $h \leftarrow hash(q)$\;
    $\alpha \leftarrow h \mod m$\;
    $k \leftarrow \rho(\floor{h / m})$\;
    bitmap[$\alpha$][$k$] $\leftarrow 1$\;
}
$R^{<j>}$ $\leftarrow$ Smallest index $i$ such that bitmap[$j$][$i$]$=0,\ 0 \leq j < m$\;
$A \leftarrow (R^{<1>} + R^{<2>} + \cdots + R^{<m>}) / m$\;
$E^* \leftarrow (m/\phi) \cdot 2^A$\;
\Return $E^*$\;
\caption{Probabilistic Counting with Stochastic Averaging algorithm}
\end{algorithm}

Choosing the parameters:
\begin{itemize}
    \item Length of the bitmaps $L$: since the probability distribution of R is a very steep distribution, we can select $L$ such that:
        $$L > log_2(n/m) + 4$$
    For example, with $m = 64$ and $L = 24$, we can count cardinalities of up to $n \sim 10^7$.
    \item Number of bitmaps $m$: the expected relative accuracy of PCSA is inversely proportional to $\sqrt{m}$, closely approximated by:
        $$0.78 / \sqrt{m}$$
    Therefore $m = 64$ leads to a standard error of about $10\%$, and with $m = 256$, the error decreases to $\sim 5\%$.
    \item When $m$ exceeds $32$, the bias compared to the standard error starts to be negligible, but for smaller values, a correcting factor can be used (in substitution of the expression for $E^*$):
        $$\Xi = m / (\phi \cdot (1 + 0.31 / m)) \cdot 2^A$$
\end{itemize}

Finally, the authors proof that the bias and standard error are, for all values of $m$, closely approximated by:
\begin{align*}
\text{bias:}\quad & 1 + 0.31/m \\
\text{standard error:}\quad & 0.78/\sqrt{m}
\end{align*}

More research has been done that shows that this algorithm is better than HyperLogLog when compressing the input data with Huffman encoding.

\subsection{LogLog algorithm} \label{subsection:loglog}

One of the drawbacks of the PCSA algorithm (Section \ref{subsection:pcsa}) is the amount of storage it requires, although it has excellent statistical properties.
Therefore a few years after publishing the PCSA algorithm, \citep{durand2003loglog} published a new algorithm which is a refinement over the high variance of the Flajolet--Martin algorithm (Section \ref{subsection:fm}): the LogLog algorithm.

The name of the algorithm comes by its ability to estimate cardinalities by using only a very small amount of additional memory, also known as memory units, where a memory unit comprises around $log log N_{max}$ bits, being $N_{max}$ an a priori upper bound of cardinalities.

The estimate of cardinality is asymptotically unbiased; with a relative accuracy (standard deviation) close to $1.30/\sqrt{m}$ for the basic LogLog, and $1.05/\sqrt{m}$ for their best version of the algorithm: Super--LogLog (Section \ref{subsection:superloglog}).

As an example, estimating cardinalities up to $N_{max}=2^{27}$ can be achieved with just $m = 2048$ memory units of $5$ bits each, ($\sim1.28$KB of auxiliary storage), with and error less than $2.5\%$.

This algorithm can be applied in a single pass since it operates incrementally, therefore we can have cardinality estimates in any given moment, and is also able to operate fully distributed and in parallel, with minimal inter-process communication.

The basic idea of the algorithm is quite similar to other algorithms previously explained in this document: given an input multiset $Q$ the cardinality of which we want to calculate, and its hashed counterpart $\mathcal{M}$, for each $x \in \mathcal{M}$ we observe the position of the least significant 1 bit ($\rho(x)$ as defined in a previous subsection).

Then, since we can expect $n/2^k$ elements in $\mathcal{M}$ to have a $\rho(x) = k$, the value:
$$R(\mathcal{M}) := \max\limits_{x \in M} \rho(x)$$
provides an indicator of $log_2n$ (with an additive bias), with $n$ being the cardinality of the multiset.

The next idea they propose consists in separating elements into $m$ groups or \enquote{buckets}, being $m$ a design parameter. If $m=2^k$, we can use the first $k$ bits to index the bucket. Afterwards, we can compute $R$ for each bucket, and with $M^{\left( j \right)}$ being the value of parameter $R$ on bucket $j$, then we can expect the arithmetic mean $\frac{1}{m}\sum_{j=1}^{m} M^{\left(j\right)}$ to approximate $log_2(n/m)$ (plus an additive bias).

LogLog can estimate $n$ as:
$$E := \alpha_m \cdot m \cdot 2^{\frac{1}{m}\sum M^{\left( j \right)}}$$

The constant $\alpha_m$ is a corrective constant for the bias of the asymptotic limit. It is derived by the authors by using the Mellin transform and the gamma function $\Gamma$:
$$\alpha_m = \left( \Gamma(-1/m) \frac{2^{-1/m}-1}{\log 2} \right)^{-m}$$

The following snippet shows the algorithm:

\begin{algorithm}[H] \label{algo:loglog}
\SetAlgoLined
\DontPrintSemicolon
\KwInput{Stream of elements $\mathcal{M}$}
\KwInput{Number of the buckets $m \equiv 2^k$}
\KwInput{Pre-computed corrective constant $\alpha_m$}
\KwOutput{Estimate, $E$, of the number of different elements in $\mathcal{M}$}
$M^{\left( j \right)} \leftarrow 0, \quad 0 \leq j < m$\;
\For {each $x \in M$} {
    $h \leftarrow hash(x)$\;
    $j \leftarrow h \mod m$\;
    $M^{\left( j \right)} \leftarrow max \left( M^{\left( j \right)}, \rho(\floor{h / m}) \right)$\;
}
$E \leftarrow \alpha_m \cdot m \cdot 2^{\frac{1}{m}\sum M^{\left( j \right)}}$\;
\Return $E$\;
\caption{LogLog algorithm}
\end{algorithm}

\if 0
An interesting point is that with limited memory, linear counting (Subsection ) proposed by Whang et al. (1990) works best with small cardinalities while the LogLog counting method works best with large cardinalities.
\fi

\subsection{Super--LogLog algorithm} \label{subsection:superloglog}

In the same paper LogLog is presented, \citep{durand2003loglog} also show how to optimize the base algorithm into what they call the Super--LogLog.

As explained before, this improved version achieves a relative accuracy (measured by the standard deviation) close to $1.05/\sqrt{m}$, which is a considerable improvement over the $1.30/\sqrt{m}$ of basic LogLog, and has also a few more advantages:
\begin{itemize}
    \item The accuracy of results is increased without any extra cost, or in other words, the dispersion of the estimates around the mean value is decreased.
    \item It improves storage utilization by allowing the the use of smaller register values (buckets).
\end{itemize}

The authors have devised the following rules for the Super--LogLog over the base LogLog algorithm:
\begin{enumerate}
    \item \textbf{Truncation Rule}: When the estimate is computed by taking the buckets' values ($m$ values in total), it is only needed to retain the $m_0 := \lfloor \theta_0 m \rfloor$ smallest values and discard the rest, where $0 \leq \theta_0 \leq 1$. According to the authors of the paper, $\theta_0 = 0.7$, produces near-optimal results.
    \item \textbf{Restriction Rule}: Instead of using the full range of bucket values (usually from $0$ to $32$, when using buckets of $5$ bits), values in the interval $\left[ 0..B \right]$ can be used, with $\lceil \log_2 \left( \frac{N_{max}}{m}\right) + 3 \rceil \leq B$. For example, with $n = 2^{27}$ and $m = 1024$, the value $B = 20$ (still has to be encoded on 5 bits) is sufficient but the difference it that now the probability that length-restriction affects the estimate decreases substantially.
\end{enumerate}

After applying those couple of rules, the pseudocode is the following:

\begin{algorithm}[H] \label{algo:superloglog}
\SetAlgoLined
\DontPrintSemicolon
\KwInput{Stream of elements $\mathcal{M}$}
\KwInput{Number of the buckets $m \equiv 2^k$}
\KwInput{Pre-computed corrective constant $\widetilde{\alpha}_m$}
\KwInput{Truncation cutoff $\theta_0$}
\KwInput{Restriction cutoff $B$}
\KwOutput{Estimate, $E$, of the number of different elements in $\mathcal{M}$}
$M^{\left( j \right)} \leftarrow 0, \quad 0 \leq j < m$\;
$m_0 \leftarrow \lfloor \theta_0 m \rfloor$\;
\For {each $x \in M$} {
    $h \leftarrow hash(x)$\;
    $j \leftarrow h \mod m$\;
    $M^{\left( j \right)} \leftarrow max \left( M^{\left( j \right)}, \rho(\floor{h / m}) \right)$\;
}
$E \leftarrow \widetilde{\alpha}_m \cdot m_0 \cdot 2^{\frac{1}{m_0}\sum^* M^{\left( j \right)}}$\;
\Return $E$\;
\caption{Super--LogLog algorithm}
\end{algorithm}

Notice the modified $\widetilde{\alpha}_m$ constant (which actually the authors do not explain how to compute) and $\sum^*$ as the truncated and restricted sum, which uses $m_0$ and only adds values $\leq B$.

\subsection{Hyper--LogLog algorithm}

Another variant of the base LogLog algorithm that \citet{flajolet2007hyperloglog} proposed is what they call Hyper--LogLog.

The key difference between Hyper--LogLog and Super--LogLog is that while Super--LogLog uses a manual version of the geometric mean (it removes the highest $(1 - \theta_0) \cdot 100\%$ buckets), the Hyper--LogLog uses the \textit{harmonic mean}. From an analytical point of view, the authors found the Super--LogLog's bias and standard error analysis more complicated.

The harmonic mean $H$ of of positive real numbers $x_1, x_2, ..., x_n$ is defined as:
$$H = \frac{n}{\frac{1}{x_1} + \frac{1}{x_2} + \cdots + \frac{1}{x_n}} = \frac{n}{\sum\limits_{i=1}^{n}\frac{1}{x_i}}$$

Hyper--LogLog is capable of estimating the cardinality with a relative accuracy (standard error)of around $1.04/\sqrt{m}$, which is also an improvement over LogLog. Moreover, LogLog's accuracy can be matched with $64\%$ less memory. As an example Hyper--LogLog can estimate cardinalities greater than $10^9$ while keeping an accuracy of $2\%$ and only using $1.5$KB.

In one hand, the \enquote{add} operation is left unchanged in respect to Super--LogLog: use the first bits of hash of the input data to select a bucket and, then keep in the selected bucket, keep the maximum index of the first bit set of the rest of the hash.

On the other hand, the significant change resides in the way $\alpha_m$ is calculated, since it has to accommodate to the use of the harmonic mean.

Therefore, given $M[i]$ the maximum value of the bucket $i$ after all the inputs have been observed:
$$M[i] = \max_{x \in S_i} \rho(x)$$

The authors devise the estimation of the cardinality $E$ as:
$$E := \alpha_m \cdot m^2 \cdot \left( \sum_{j=1}^{m} 2^{-M[j]} \right) = \frac{\alpha_m \cdot m^2}{\sum_{j=1}^{m} 2^{-M[j]}}$$

With the constant $\alpha_m$ defined as:

$$\alpha_m := \left( m \int_{0}^{\infty} \left( log_2 \left( \frac{2 + u}{1 + u} \right) \right)^m du\right)^{-1}$$

Since this constant is not trivial to calculate in this algorithm, the authors provide precomputed values for typical global configurations of it, which are maximal cardinalities in the range $\left[0..10^9\right]$ and values for the number of buckets $m = 2^4,...,2^{16}$.

Further analytical analysis shows the authors that Hyper--LogLog is biased for small cardinalities (below $\frac{5}{2}m$), and that the Linear Counting estimator (Subsection \ref{subsection:linearcounting}), $E^* = m \cdot ln \left( \frac{m}{V} \right)$, ($V$ is the count of registers equal to 0), can be used.

The following figure shows the pseudocode found in the author's paper:
\begin{figure}[H]
\includegraphics[width=\linewidth]{img/hyperloglog_pseudocode.png}
\caption{Hyper--LogLog pseudocode for common cases. Credit: \citet{flajolet2007hyperloglog}}
\label{fig:hyperloglog_pseudocode}
\end{figure}


\subsection{More algorithms}

As explained in the introduction, the count-distinct problem is extensively used nowadays, and even more considering that we are in the age of \enquote{big data}, where the cardinalities of sets are in the order of millions or even billions.

Therefore, extensive research is still being done on this area, and newer and improved algorithms have been proposed.

One case of such improvement is HyperLogLog++, developed by Google researches \citet{HyperLogLogpp}, which allows to use $64$-bit hash functions and also mitigates a bias found for small cardinalities.

Another approach is what \citet{selflearningbitmap} call \enquote{Self-Learning Bitmap}, which uses a binary vector whose entries are updated by an adaptive sampling process for inferring the cardinality, where the sampling rates are reduced sequentially as more and more entries change.

\section{Implementation}

Other than researching algorithms for the count-distinct problem, another important part of the project has been their implementation.

I have chosen the language C++, and in particular the C++11 standard, therefore in order to compile it, a C++11 capable compiler such as GCC\footnote{\url{https://gcc.gnu.org/}} or \textit{clang}\footnote{\url{https://clang.llvm.org/}} is needed.

Valgrind\footnote{\url{http://www.valgrind.org/}} has also been used to check that there are no memory leaks in my implementation.

In order to keep the code clean, I have decided to keep the implementation of the algorithms self-contained in a single header file (\texttt{.hpp}). All of them inherit from a templatized by typename base class (virtual interface) that I have called \texttt{ICountDistinctAlgorithm<T>}, and have the following methods which all the algorithms derived from it have to implement:
\begin{itemize}
    \item \texttt{void add(const T\& val)}: Observes a new item from the set to calculate the cardinality of.
    \item \texttt{uint64\_t count() const}: Returns the calculated cardinality. Might be exact or an estimate depending on the algorithm.
    \item \texttt{void merge(const ICountDistinctAlgorithm<T>\& other)}: Merges two different instances of the same algorithm type.
    \item \texttt{void reset()}: Resets the algorithm metadata.
    \item \texttt{std::string name()}: Returns the algorithm's name.
\end{itemize}

As the hash function, initially I considered using MurmurHash3\footnote{\url{https://github.com/aappleby/smhasher/blob/master/src/MurmurHash3.cpp}} but I have ended up using C++'s \texttt{std::hash<T>} template struct\footnote{\url{https://en.cppreference.com/w/cpp/utility/hash}}, since it already provided implementations for many data types.

\section{Experiments}

For the experiments I am considering two kinds of data sources:
\begin{itemize}
    \item Random data: Generated by using C++'s \texttt{std::uniform\_int\_distribution}.
    \item A text (non-random data): The objective is to count how many different words there are in the text, which I have downloaded from this web page\footnote{\url{https://introcs.cs.princeton.edu/java/data/}}.
\end{itemize}

The program has been compiled with flags \texttt{-O3} and \texttt{-march=native} to generate code optimized for the machine the experiments will be run on, which has the following specifications:
\begin{itemize}
    \item Intel(R) Core(TM) i7-8750H CPU @ 2.20GHz (CPU max MHz: 4100.0)
    \item 16GiB DDR4 2667 MHz
\end{itemize}

\subsection{Random data}

The first experiment I have done is to generate a vector of increasing sizes of random data, then run the algorithms and compare them against the Naive, which gives the exact cardinality. The sizes of the input data go from $100000$ to $10000000$, with an step of $100000$.

The following image shows the relative difference, calculated as \\ $cardinality_{algorithm} / cardinality_{naive}$, for all the algorithms against the exact cardinality (Naive algorithm):
\begin{figure}[H]
\includegraphics[width=\linewidth]{img/Random_ relative difference against Naive algorithm (exact).png}
\end{figure}

Unfortunately, as can be seen in the picture, my implementation of the Flajolet--Martin algorithm seems to be slightly wrong, as the error is higher than the expected one binary order of magnitude. Another disappointing point is my implementation of the Super--LogLog algorithm, because although it seems to have a really good tendency, its estimator seems to be multiplied by the wrong correcting factor ($\widetilde{\alpha}_m$), which makes sense since the authors do not provide a way of calculating it in the paper.

To have a clearer picture on other algorithms, the following picture is zoomed to differences between $0.8$ and $1.2$:

\begin{figure}[H]
\includegraphics[width=\linewidth]{img/Random_ relative difference against Naive algorithm (exact), zoomed to 0.8-1.2.png}
\end{figure}

Now this image provides what I think are really good results: almost all the algorithms stay within a $10\%$ of error margin, and LinearCounting shows almost perfect cardinality estimations, which makes sense since its memory requirements are pretty much linear in comparison of the number of elements in the set. On the other hand it seems LogLog is the one with the most variation and distance from the Naive.

The next couple of images show one of the strengths of the probabilistic algorithms, they are generally much faster than exact ones: 
\begin{figure}[H]
\includegraphics[width=\linewidth]{img/Random_ cardinality estimation time.png}
\end{figure}

As we can see the time for the Naive algorithm seems to increase linearly on the size of the input data, where the time increase of the rest algorithms at this scale is barely noticeable. The next figure shows the same plot but without the Naive algorithm:
\begin{figure}[H]
\includegraphics[width=\linewidth]{img/Random_ cardinality estimation time (without Naive).png}
\end{figure}

Again, such fast cardinality estimations, less than $0.03$ seconds, for data sets of as many as $10000000$ elements is quite impressive.

\subsection{Text (non-random)}

Now I repeat the same measurements, cardinality and time, but instead of using random data, the input will be real texts downloaded from the website mentioned before.

I will consider the cardinality the number of different words in the text. The following table shows for each text, the \textit{total} number of words:

\begin{center}
\begin{tabular}{|l|r|} 
\hline
Filename                    & Total \#words \\
\hline
\texttt{dickens.txt}        & 5158644 \\ 
\texttt{leipzig1m.txt}      & 21191455 \\ 
\texttt{mobydick.txt}       & 209341 \\ 
\texttt{t8.shakespeare.txt} & 901325 \\ 
\texttt{upc-items.csv}      & 5372259 \\ 
\hline
\end{tabular}
\end{center}

In this first plot, which shows the relative difference against Naive, we see again that Flajolet--Martin and Super--LogLog are not working as expected. Other than that, the rest of the algorithms show a close estimation to the exact cardinality on each of the texts.
\begin{figure}[H]
\includegraphics[width=\linewidth]{img/Text_ relative difference against Naive algorithm (exact).png}
\end{figure}

This plot is a zoomed-in version of the previous one with Flajolet--Martin and Super--LogLog removed. We can see how LogLog seems to be the algorithm (that is working) that performs the worst.
\begin{figure}[H]
\includegraphics[width=\linewidth]{img/Text_ relative difference against Naive algorithm (exact), zoomed.png}
\end{figure}

Regarding the cardinality estimation time, we see again the big difference between exact and probabilistic algorithms:
\begin{figure}[H]
\includegraphics[width=\linewidth]{img/Text_ cardinality estimation time.png}
\end{figure}

When zoomed, we also see that all the probabilistic algorithms take roughly the same time:
\begin{figure}[H]
\includegraphics[width=\linewidth]{img/Text_ cardinality estimation time, zoomed.png}
\end{figure}

\section{Conclusions}

I think probabilistic data structures and algorithms play a fundamental role in being able to solve problems, specially on huge data sets or in environments where performance or storage is a limitation.

Of course, before blindly using this class of algorithms one should check by theoretical analysis that the error can be controlled and it will be within the correct limits.

The experiments also helped me reaffirm my thoughts, even though I was not fully successful when implementing a couple of the explained algorithms.

\newpage

\appendix
\section{Instructions}

Once compiled with \texttt{make}, a single executable file called \texttt{count-distinct} is generated, which has the following usage:
\begin{lstlisting}[language=bash,basicstyle=\small\ttfamily]
$ ./count-distinct -h
count-distinct by Sergi Granell (sergi.granell@est.fib.upc.edu)
Usage:
	./count-distinct <type> <arg> [num_runs]
Where <type> can be:
	-r for random.      <arg> is the number of elements.
	-f for a text file. <arg> is the filename.
	-h to show this message and exit.
And <num_runs> is the number of runs (arithmetic mean).
The default is:
	./count-distinct -r 1000000 1
\end{lstlisting}

As can be seen, the program has two modes of execution: the random mode and the text file mode.
When running in random mode (\texttt{-r}), the number of random elements to generate can be passed.
If running in text file mode (\texttt{-f}), then the next argument is the path of the filename from where to load the words from.

The last argument is the number of runs to execute. The output time is the average of the times of each run.

When executed, it will run each of the algorithms (first column) on the data-set; and it will report both the cardinality (second column) and the execution time (third column), as can be seen in the following execution trace:
\begin{lstlisting}[language=bash,basicstyle=\small\ttfamily]
$ ./count-distinct -f ../data/dickens.txt 5
File mode with file "../data/dickens.txt".
Input data size: 5158644
Number of runs: 5
Naive   	    184705	0.27884673
LinearCounting	    184537	0.06796786
FlajoletMartin	    338902	0.06877796
PCSA    	    190436	0.07497455
LogLog  	    172144	0.07318087
SuperLogLog	     64632	0.07266680
HyperLogLog	    184446	0.07525298
\end{lstlisting}

After compiling, another executable called \texttt{experiment} is also generated, which basically runs all the experiments automatically.

\newpage

\bibliographystyle{plain}
\bibliography{references}
\end{document}
