#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <cmath>
#include <chrono>

template <typename T>
int compare(T a, T b)
{
	return (a < b) ? -1 : (a > b) ? 1 : 0;
}

/*
 * left is the left index for the interval
 * right is the right index for the interval
 * k is the desired index value, where array[k] is the (k+1)th smallest element when left = 0
 */
template <typename T>
void fr_select_i(std::vector<T> &array, size_t k, size_t left, size_t right, size_t m1, float m2)
{
	while (right > left) {
		/*
		 * use select recursively to sample a smaller set of size s
		 * the arbitrary constants 600 (m1) and 0.5 (m2) are used in the original
		 * version to minimize execution time
		 */
		if (right - left > m1) {
			size_t n = right - left + 1;
			size_t m = k - left + 1;
			float z = std::log(n);
			float s = m2 * std::exp(2 * z / 3);
			float sd = m2 * std::sqrt(z * s * (n - s) / n) * (m - n / 2 < 0 ? -1 : 1);
			size_t new_left = std::max(left, (size_t)std::floor(k - m * s / n + sd));
			size_t new_right = std::min(right, (size_t)std::floor(k + (n - m) * s / n + sd));
			fr_select_i(array, k, new_left, new_right, m1, m2);
		}

		/* partition the elements between left and right around t */
		T t = array[k];
		size_t i = left;
		size_t j = right;

		std::swap(array[left], array[k]);
		if (compare(array[right], t) > 0)
			std::swap(array[left], array[right]);

		while (i < j) {
			std::swap(array[i], array[j]);
			i++;
			j--;
			while (compare(array[i], t) < 0)
				i++;
			while (compare(array[j], t) > 0)
				j--;
		}

		if (compare(array[left], t) == 0) {
			std::swap(array[left], array[j]);
		} else {
			j++;
			std::swap(array[j], array[right]);
		}

		/*
		 * adjust left and right towards the boundaries of the subset
		 * containing the (k - left + 1)th smallest element
		 */
		if (j <= k)
			left = j + 1;
		if (k <= j)
			right = j - 1;
	}
}

/*
 * The kth element in the array will contain the kth smallest value
 */
template <typename T>
void fr_select(std::vector<T> &array, size_t k, size_t m1, float m2)
{
	fr_select_i(array, k, 0, array.size(), m1, m2);
}

/*
 * argv[1] = seed
 * argv[2] = array length
 * argv[3] = k
 */
int main(int argc, char *argv[])
{
	unsigned int seed = 42;
	size_t n = 1000000000;
	size_t k =  n / 2;
	size_t m1 = 600;
	float m2 = 0.5;

	if (argc > 1)
		seed = std::stoul(argv[1]);
	if (argc > 2)
		n = std::stoul(argv[2]);
	if (argc > 3)
		k = std::stoul(argv[3]);
	if (argc > 4)
		m1 = std::stoul(argv[4]);
	if (argc > 5)
		m2 = std::stof(argv[5]);

	std::cout << "Parameters:" << std::endl;
	std::cout << "  seed: " << seed << std::endl;
	std::cout << "     n: " << n << std::endl;
	std::cout << "     k: " << k << std::endl;
	std::cout << "    m1: " << m1 << std::endl;
	std::cout << "    m2: " << m2 << std::endl;

	std::srand(seed);

	std::cout << "Initializing vector..." << std::endl;
	/* Generate random vector */
	std::vector<int> v(n);
	for (size_t i = 0; i < n; i++)
		v[i] = std::rand();

	std::cout << "Start!" << std::endl;
	/* Start time */
	auto start = std::chrono::high_resolution_clock::now();

	/* Run algorithm */
	fr_select(v, k, m1, m2);

	/* End time */
	auto finish = std::chrono::high_resolution_clock::now();
	double elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(finish - start).count() / (double)1000000000;

	std::cout << "End!" << std::endl;
	std::cout << elapsed << " s" << std::endl;

	/* Print result */
	std::cout << "Floyd-Rivest " << k << "-th:  " << v[k] << std::endl;

#if 0
	/* Check if the result is correct */
	std::sort(v.begin(), v.end());
	std::cout << "After sorting "<< k << "-th: " << v[k] << std::endl;
#endif

	/* Print easily-parable results to stderr */
	std::cerr << elapsed << std::endl;

	return 0;
}
