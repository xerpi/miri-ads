#!/bin/bash

program=frselect

m1s=(200 400 600 800 1000)
m2s=(0.25 0.5 0.75 1)

make "$program"

for m1 in "${m1s[@]}"
do
	for m2 in "${m2s[@]}"
	do
		echo "m1: $m1, m2: $m2"

		./$program 42 1000000000 500000000 $m1 $m2 2>&1 > /dev/null
	done
done
